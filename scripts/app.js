let pay = 0;        // accumulated hourly pay
let balance = 0;    // account balance
let loan = 0;       // debt
let laptops = [];   // fetched computers
let selectedLaptop; // laptop selected in drop down

/* Money elements referenced at multiple places */
const payElement = document.getElementById("pay")
const balanceElement = document.getElementById("balance")
const loanedElement = document.getElementById("loaned")
const loanAmountElement = document.getElementById("loan")

/* Computer Display elements referenced at multiple places */
const titleElement = document.getElementById("laptop_title")
const descriptionElement = document.getElementById("laptop_desc")
const priceElement = document.getElementById("laptop_price")
const imageElement = document.getElementById("laptop_image")

import { clamp } from "./modules/utils.js"

/* Fetch laptops from API */
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(function(response) {
        return response.json(); // convert to object array
    })
    .then(function(result) {
        laptops = result;       // store laptops

        const dropdown = document.getElementById("computers")   // get dropdown element

        for (const laptop of laptops) {    
            const option = document.createElement("option")     // create an option for each laptop

            option.innerHTML = laptop.title;                    // set option text to laptop title
            
            dropdown.appendChild(option)                        // add to dropdown field
        }

        UpdateLaptopDisplay(laptops[0])                         // default display and selection (first laptop)
    })
    .catch((e) => alert(`Error loading computers!\n${e}`))      // something went wrong!?

/* Select a laptop and update display */
function UpdateLaptopDisplay(laptop) {
    selectedLaptop = laptop                                     // update selection

    const features = []                                         
    
    laptop.specs.forEach(spec => {                  
        const li = document.createElement("li")     // create list item
        li.innerHTML = spec                         // with corresponding spec as inner html
        
        features.push(li)                           // add to list of features 
    });

    /* Clear features display */
    const featsDisplay = document.getElementById("features");
    featsDisplay.replaceChildren([]);

    /* Add each feature to features display */
    features.forEach(value =>                       
        featsDisplay.appendChild(value)             
    )

    /* Update laptop display */
    titleElement.innerHTML = laptop.title           
    descriptionElement.innerHTML = laptop.description
    priceElement.innerHTML = laptop.price           

    imageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${laptop.image}`
}

/* Update display when changing selection in dropdown */
document.getElementById("computers").addEventListener("change", (e) => {
    UpdateLaptopDisplay(laptops[e.target.selectedIndex])
})

/* Pay button - hourly pay of 100 */
document.getElementById("workBtn").addEventListener("click", () => {
    pay += 100;

    /* Update pay element text */
    payElement.innerHTML = pay 
})

/* Pay button */
document.getElementById("payBtn").addEventListener("click", () => {
    const toLoan = clamp(pay * .1, .0, loan)    // deduct 10% (but at most equal to loan)

    loan -= toLoan;             // pay off some loan
    balance += pay - toLoan;    // add the rest to balance
    pay = 0;                    // reset pay

    /* Update money elements  */
    payElement.innerHTML = pay  
    balanceElement.innerHTML = balance
    loanedElement.innerHTML = loan

    /* Hide loan info if fully paid */
    loanedElement.hidden = loanAmountElement.hidden = document.getElementById("repayBtn").hidden = loan == 0
})

/* Repay loan button */
document.getElementById("repayBtn").addEventListener("click", () => {

    /* Pay with balance and reset loan */
    balance -= loan;
    loan = 0;
    
    /* Update money elements */
    loanedElement.innerHTML = loan
    balanceElement.innerHTML = balance
    
    /* Hide loan info if fully paid */
    loanedElement.hidden = loanAmountElement.hidden = document.getElementById("repayBtn").hidden = loan == 0
})

/* Get Loan button */
document.getElementById("loanBtn").addEventListener("click", () => {
    if (loan > 0) {
        return alert("You already have an active loan!")
    }

    /* Amount of loan requested */
    const amount = parseInt(prompt("How much would you like to borrow?"));
    
    /* Check if input was valid */
    if (isNaN(amount) || amount <= 0) 
        return;

    if (amount > balance * 2) 
        return alert(
            `Cannot get a loan of that amount!\n
            You are not allowed to get a loan of 
            more than double your balance.`
        );
    else if (loan > 0)  // loan is not zero -> there is an active loan
        return alert("You may not have more than one active loan.");

    alert(`You now have a debt of ${amount}.`);

    /* Set active loan and add amount to spendable balance */
    loan = amount;
    balance += amount;

    /* Update */
    balanceElement.innerHTML = balance
    loanedElement.innerHTML = loan
    
    /* Show loan elements */
    loanedElement.hidden = loanAmountElement.hidden = document.getElementById("repayBtn").hidden = false
})

/* Buy laptop button */
document.getElementById("buyBtn").addEventListener("click", () => {
    if (selectedLaptop.price > balance)
        return alert("Insufficient funds!")

    balanceElement.innerHTML = balance -= selectedLaptop.price
    alert(`You are now the owner of '${selectedLaptop.title}'!`)
})