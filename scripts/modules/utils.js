/* 
    Clamps a value between min and max 
    (no built in?) 
*/
export function clamp(value, min, max) {
    if (value < min) return min;
    else if (value > max) return max;

    return value; // return value if in range
}